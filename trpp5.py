def from_c_to_k(tem):
    return round(tem + 273)
def from_c_to_f(tem):
    return round(tem * 9 / 5 + 32)
def from_f_to_c(tem) :
    return round((tem - 32) * 5 / 9)
def from_f_to_k(tem):
    return round(from_f_to_c(tem) + 273)
def from_k_to_c(tem) :
    return round(tem - 273)
def from_k_to_f(tem):
    return from_c_to_f(from_k_to_c(tem))


def conversation (temperature, from_scale, to_scale):
    temp_scales = {'C','F','K'}
    from_scale = from_scale.upper()
    to_scale = to_scale.upper()

    file = open("test.txt", "a")

    temp = 0
    
    if (from_scale not in temp_scales) or (to_scale not in temp_scales):
        file.write("Второй и третий параметр только: C,F,K\n")
        return "Второй и третий параметр только: C,F,K"
    if type(temperature) != int:
        file.write("Первым параметром должно быть число")
        return "Первым параметром должно быть число"
    if from_scale == "C":
        if to_scale == "F":
            temp = from_c_to_f(temperature)
        if to_scale == "K":
            temp = from_c_to_k(temperature)
    if from_scale == "F":
        if to_scale == 'C':
            temp = from_f_to_c(temperature)
        if to_scale == "K":
            temp = from_f_to_k(temperature)
    if from_scale == "K":
        if to_scale == "C":
            temp = from_k_to_c(temperature)
        if to_scale == "F":
            temp = from_k_to_f(temperature)
    if from_scale == to_scale:
        temp = temperature

    file.write(str(temperature) + from_scale +" = "+ str(temp) + to_scale + "\n")

    return temp

if __name__ == "__main__":
    
    while True:
        try:
            temp = int(input())
        except ValueError:
            print("Error! Это не число, попробуйте снова.")
        else:
            break
    fr = input()
    fa = input()
    
    print(conversation(temp,fr,fa))
