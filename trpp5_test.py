from trpp5 import conversation

def test_c_to_k():
    assert conversation(10,'C','K') == 283
    
def test_k_to_c():
    assert conversation(283,'K','C') == 10

def test_c_to_f():
    assert conversation(10,'C','F') == 50

def test_f_to_c():
    assert conversation(50,'F','C') == 10

def test_k_to_f():
    assert conversation(300,'K','F') == 81

def test_f_to_k():
    assert conversation(81,'F','K') == 300

def test_not_exist_scale():
    assert conversation(1,'asf','asfas') == 'Второй и третий параметр только: C,F,K'

def test_not_number():
    assert conversation('awdwa','K','C') == "Первым параметром должно быть число"
